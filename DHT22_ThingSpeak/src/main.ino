/********************************************************************************************/
/*  
 *Sensado y Uploading a ThingSpeak
 *
 *Medimos los valores de Temperatura y Humedad y los subimos a ThingSpeak para visualizarlos.
*/
/********************************************************************************************/
/********************************************************************************************/
//Librerias
/********************************************************************************************/

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <DHTNew.h>
#include "ThingSpeak.h"

/********************************************************************************************/
//Variables
/********************************************************************************************/

const char* ssid = "Speedy-Fibra-4589"; // Enter your WiFi name

const char* password = "Nico_2017"; // Enter the password

unsigned long myChannelNumber = 475515;

const char * myWriteAPIKey = "LPC7NDT9GJPV72HL";

DHT dht(2, DHT_MODEL_DHT22);

unsigned long dhtReadings = 0;

unsigned long dhtErrors = 0;

WiFiClient client;

/********************************************************************************************/
//Setup Principal
/********************************************************************************************/

void setup(void) {

    Serial.begin(9600);

    dht.begin();

    WiFi.mode(WIFI_STA);

    WiFi.begin(ssid, password);

    Serial.println("");

    while (WiFi.status() != WL_CONNECTED) {
        
        delay(500);
        
        Serial.print(".");
    }
    
    Serial.println("");
    Serial.print("Connected to ");
    Serial.println(ssid);
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());

    ThingSpeak.begin(client);  // Initialize ThingSpeak
}

/********************************************************************************************/
//Main
/********************************************************************************************/

void loop(void) {

    float Hum = 0, Temp = 0;
  
    // make a reading attempt (i.e. returns true every getMinimumSamplingPeriod() ms minimum)
    if (dht.read()) {
    
        dhtReadings++;

        if (dht.getError() != DHT_ERROR_NONE) { // report errors

            dhtErrors++;
            Serial.print("Error: ");
            Serial.println(dht.getErrorString());
        }

        else {

            //Obtenemos los valores del sensor
            Temp = dht.getTemperature();
            Hum = dht.getHumidity();

            //Cargamos los valores en sus campos correspondientes
            ThingSpeak.setField(1, Temp);
            ThingSpeak.setField(2, Hum);

            //Enviamos los datos a nuestro canal
            int x = ThingSpeak.writeFields(myChannelNumber, myWriteAPIKey);
            
            if(x == 200){
                
                Serial.println("Channel update successful.");
            }
            
            else{
                
                Serial.println("Problem updating channel. HTTP error code " + String(x));
            }

            /*if (client.connect(ThingSpeakServer,80)) {  //   Server api.thingspeak.com  
            
                String postStr = apiKey;
                postStr +="&field1=";
                postStr += String(Temp);
                postStr +="&field2=";
                postStr += String(Hum);
                postStr += "\r\n\r\n";

                client.print("POST /update HTTP/1.1\r\n");
                client.print("Host: api.thingspeak.com\r\n");
                client.print("Connection: close\r\n");
                client.print("X-THINGSPEAKAPIKEY: "+apiKey+"\r\n");
                client.print("Content-Type: application/x-www-form-urlencoded\r\n");
                client.print("Content-Length: ");
                client.print(postStr.length());
                client.print("\r\n\r\n");
                client.print(postStr);

                Serial.print("Temperatura: ");
                Serial.print(Temp);
                Serial.print(" C, Humedad: ");
                Serial.print(Hum);
                Serial.println("%. Enviado a Thingspeak.");
            }

            client.stop();*/

            Serial.println("Esperando...");

            delay(30000); // thingspeak needs minimum 15 sec delay between updates
        }
    }

    else { Serial.println("Error en la lectura del sensor!"); }
}

/********************************************************************************************/