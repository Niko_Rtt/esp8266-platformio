/*****************************************************************************
* Cartel de DEFCON
*
* Autor: Nicolas Rasitt
* Fecha: 15/02/19
* Version: 1.0.0
*
*****************************************************************************/

/*==================[inclusions]=============================================*/

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <NeoPixelBrightnessBus.h>

/*==================[definitions]============================================*/

#define NUM_LEDS 8 // Número de píxeles
#define DATA_PIN D1 // Pin de datos
#define RED 1
#define BLUE 2
#define GREEN 3
#define NO_COLOR 4
#define ORANGE 5

/*==================[global variables]======================================*/

ESP8266WebServer server;
char* ssid = "Speedy-Fibra-4589";
char* password = "Nico_2017";

// Colores
RgbColor Red(0, 255, 0);
RgbColor Green(255, 0, 0);
RgbColor Blue(0, 0, 255);
RgbColor Orange(70, 255, 0);
RgbColor Yellow(255, 255, 0);
RgbColor White(255, 255, 255);
RgbColor No_Color(0, 0, 0);

// Declaramos objeto
NeoPixelBrightnessBus<NeoRgbFeature, Neo800KbpsMethod> strip(NUM_LEDS, DATA_PIN);

/*==================[internal functions declararion]========================*/

void LevelDefcon1 (void);
void LevelDefcon2 (void);
void LevelDefcon3 (void);
void LevelDefcon4 (void);
void LevelDefcon5 (void);
void LEDS_NEOPIXEL_Init (void);
void LEDS_NEOPIXEL_Set (uint8_t Pixel, uint8_t Color);
void LEDS_NEOPIXEL_Check (void);

/*==================[Hardware Setup]=======================================*/

void setup(){

	LEDS_NEOPIXEL_Init();

	LEDS_NEOPIXEL_Check();

	WiFi.begin(ssid,password);

	Serial.begin(9600);

	while(WiFi.status()!=WL_CONNECTED){

		Serial.print(".");
		delay(500);
	}

	Serial.println("");
	Serial.print("IP Address: ");
	Serial.println(WiFi.localIP());

	server.on("/",[](){server.send(200,"text/plain","Defcon Activated");});
	server.on("/defcon/1",LevelDefcon1);
	server.on("/defcon/2",LevelDefcon2);
	server.on("/defcon/3",LevelDefcon3);
	server.on("/defcon/4",LevelDefcon4);
	server.on("/defcon/5",LevelDefcon5);
	server.begin();
}

/*==================[Main Progam]==========================================*/

void loop(){

	server.handleClient();
}

/*==================[internal functions definition]==========================*/

void LevelDefcon1(){

	strip.ClearTo(White);

    strip.Show(); // Actualizamos el píxel

	server.send(200,"text/plain","Defcon 1 Active");
}

//******************************

void LevelDefcon2(){

	strip.ClearTo(Red);

    strip.Show(); // Actualizamos el píxel

	server.send(200,"text/plain","Defcon 2 Active");
}

//******************************

void LevelDefcon3(){

	strip.ClearTo(Yellow);

    strip.Show(); // Actualizamos el píxel

	server.send(200,"text/plain","Defcon 3 Active");
}

//******************************

void LevelDefcon4(){

	strip.ClearTo(Green);

    strip.Show(); // Actualizamos el píxel

	server.send(200,"text/plain","Defcon 4 Active");
}

//******************************

void LevelDefcon5(){

	strip.ClearTo(Blue);

    strip.Show(); // Actualizamos el píxel

	server.send(200,"text/plain","Defcon 5 Active");
}

//******************************

void LEDS_NEOPIXEL_Init (void){

    strip.Begin(); // Iniciamos la tira

    strip.Show();
}

//******************************

void LEDS_NEOPIXEL_Set (uint8_t Pixel, uint8_t Color){

    if(Color == RED){ strip.SetPixelColor(Pixel, Red); }

    else if(Color == BLUE){ strip.SetPixelColor(Pixel, Blue); }

    else if(Color == GREEN){ strip.SetPixelColor(Pixel, Green); }

    else if(Color == ORANGE){ strip.SetPixelColor(Pixel, Orange); }

    else if(Color == NO_COLOR){ strip.SetPixelColor(Pixel, No_Color); }

    strip.Show();
}

//******************************

void LEDS_NEOPIXEL_Check (void){

    strip.ClearTo(Orange);

    strip.Show(); // Actualizamos el píxel

    delay(400);

    strip.ClearTo(No_Color);

    strip.Show(); // Actualizamos el píxel
}

/*==================[end of file]============================================*/