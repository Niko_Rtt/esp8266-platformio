/********************************************************************************************/
/*  
 *Hello from ESP8266
 *
 *Creamos un server en el ESP8266 y a cada cliente le respondemos con un mensaje.
*/
/********************************************************************************************/
/********************************************************************************************/
//Librerias
/********************************************************************************************/

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

/********************************************************************************************/
//Variables
/********************************************************************************************/

const char* ssid = "Speedy-Fibra-4589"; // Enter your WiFi name

const char* password = "Nico_2017"; // Enter the password

ESP8266WebServer server(80);

/********************************************************************************************/
//Funciones
/********************************************************************************************/

void handleRoot() {
    
    digitalWrite(LED_BUILTIN, HIGH);
    
    server.send(200, "text/plain", "Hola desde ESP8266 a Arduino Day!");

    Serial.println("Server Respondio.");

    digitalWrite(LED_BUILTIN, LOW);
}

void handleNotFound() {

    digitalWrite(LED_BUILTIN, HIGH);

    String message = "File Not Found\n\n";

    message += "URI: ";
    message += server.uri();
    message += "\nMethod: ";
    message += (server.method() == HTTP_GET) ? "GET" : "POST";
    message += "\nArguments: ";
    message += server.args();
    message += "\n";
    
    for (uint8_t i = 0; i < server.args(); i++) {
        message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
    }
    
    server.send(404, "text/plain", message);

    Serial.println("Server Error.");
    
    digitalWrite(LED_BUILTIN, LOW);
}

/********************************************************************************************/
//Setup Principal
/********************************************************************************************/

void setup(void) {

    pinMode(LED_BUILTIN, OUTPUT);

    digitalWrite(LED_BUILTIN, LOW);

    Serial.begin(9600);

    WiFi.mode(WIFI_STA);

    WiFi.begin(ssid, password);

    Serial.println("");

    while (WiFi.status() != WL_CONNECTED) {
        
        delay(500);
        
        Serial.print(".");
    }
    
    Serial.println("");
    Serial.print("Connected to ");
    Serial.println(ssid);
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());

    if (MDNS.begin("esp8266")) {
        
        Serial.println("MDNS responder started");
    }

    server.on("/", handleRoot);

    server.on("/inline", [](){ server.send(200, "text/plain", "this works as well"); });

    server.onNotFound(handleNotFound);

    server.begin();

    Serial.println("HTTP server started");
}

/********************************************************************************************/
//Main
/********************************************************************************************/

void loop(void) {
  
    server.handleClient(); //Unica funcion que se encarga de atender los pedidos de los clientes.

}

/********************************************************************************************/