/********************************************************************************************/
/*  
 *WIFI WEB Server
 *
 *Creamos un server en el ESP8266 y cada cliente podra enviar un mensaje para activar un gpio.
 *http://server_ip/led/LOW apaga el led,
 *http://server_ip/led/HIGH prende al led.
*/
/********************************************************************************************/
/********************************************************************************************/
//Librerias
/********************************************************************************************/

#include <Arduino.h>
#include <ESP8266WiFi.h>

/********************************************************************************************/
//Variables
/********************************************************************************************/

const char* ssid = "Speedy-Fibra-4589"; // Enter your WiFi name

const char* password = "Nico_2017"; // Enter the password

WiFiServer server(80);

/********************************************************************************************/
//Setup Principal
/********************************************************************************************/

void setup() {

    pinMode(LED_BUILTIN, OUTPUT);

    digitalWrite(LED_BUILTIN, LOW);

    Serial.begin(9600);

    WiFi.mode(WIFI_STA);

    WiFi.begin(ssid, password);

    Serial.println("");

    while (WiFi.status() != WL_CONNECTED) {
        
        delay(500);
        
        Serial.print(".");
    }
    
    Serial.println("");
    Serial.print("Connected to ");
    Serial.println(ssid);
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());

    server.begin(); // Start the server

    Serial.println("Server iniciado.");
}

/********************************************************************************************/
//Main
/********************************************************************************************/

void loop() {

    bool flag_Contestar = false;

    uint8_t Timeout = 0, Timer = 0;

    WiFiClient client = server.available();// Check if a client has connected

    if (!client) {

        client.stop();

        delay(250);

        flag_Contestar = false;
    }

    else {

        Serial.println("Nuevo Cliente"); 

        while (!client.available() && Timeout == 0) { 
            
            delay(1); // Wait until the client sends some data

            Timer++;

            if(Timer == 250){ Timeout = 1;} //Or escape if there is no response or the client haven´t send data
        }

        if(Timeout){

            Serial.println("No envio nada el cliente. Desconectando.");
            
            client.stop();

            delay(1000);

            flag_Contestar = false;
        }

        else{ 

            // Read the first line of the request
            String req = client.readStringUntil('\r');

            Serial.println(req);
            
            client.flush();

            // Match the request
            int val;
            
            if (req.indexOf("/led/LOW") != -1) { val = LOW; flag_Contestar = true; }
            
            else if (req.indexOf("/led/HIGH") != -1) { val = HIGH; flag_Contestar = true; }
            
            else {
                
                Serial.println("Pedido invalido");
                
                client.stop();

                delay(1000);

                flag_Contestar = false;
            }

            if(flag_Contestar == true){

                digitalWrite(LED_BUILTIN, val);

                client.flush();

                // Prepare the response
                String s = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n<!DOCTYPE HTML>\r\n<html>\r\nEl led paso al estado ";
                s += (val) ? "high" : "low";
                s += "</html>\n";

                // Send the response to the client
                client.print(s);
                
                delay(500);
                
                Serial.println("Client disonnected");

                client.stop();

                delay(1000);
            }
        }
    }
}

/********************************************************************************************/