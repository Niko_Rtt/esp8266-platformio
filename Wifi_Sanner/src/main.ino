/********************************************************************************************/
/*  
 *Wifi Scanner
 *
 *Desconectamos al ESP8266 de cualquier punto AP e imprimos las redes disponibles.
*/
/********************************************************************************************/
/********************************************************************************************/
//Librerias
/********************************************************************************************/

#include <Arduino.h>
#include "ESP8266WiFi.h"

/********************************************************************************************/
//Setup Principal
/********************************************************************************************/

void setup() {

    Serial.begin(9600);

    WiFi.mode(WIFI_STA); // Set WiFi to station mode and disconnect from an AP if it was previously connected
    
    WiFi.disconnect();
    
    delay(100);

    Serial.println("Escaner Inicilizado.");
}
/********************************************************************************************/
//Main
/********************************************************************************************/

void loop() {

    Serial.println("Iniciando escaneo...");

    int n = WiFi.scanNetworks();

    Serial.println("Escaneo terminado.");
    
    if (n == 0) {
        
        Serial.println("Ningun red encontrada.");
    }
    
    else {
        
        Serial.print(n);
        
        Serial.println(" redes encontradas.");
        
        for (int i = 0; i < n; ++i) { // Print SSID and RSSI for each network found
            
            Serial.print(i + 1);
            Serial.print(": ");
            Serial.print(WiFi.SSID(i));
            Serial.print(" (");
            Serial.print(WiFi.RSSI(i));
            Serial.print(")");
            Serial.println((WiFi.encryptionType(i) == ENC_TYPE_NONE) ? " " : "*");
        }
    }

    Serial.println("");

    delay(5000);
}

/********************************************************************************************/